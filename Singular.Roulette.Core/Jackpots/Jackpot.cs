﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Core.Jackpots
{
    public class Jackpot
    {
        public int Id { get; set; }
        public long TotalJackpotAmountInCentsAsString { get; set; }

        public DateTime LastModifiedDate { get; set; }
        public DateTime? LastDrawDate { get; set; }
        public JackpotType Type { get; set; }
        public byte[] Version { get; set; }
    }

    public enum JackpotType
    {
        VirtualRoullete = 1,
    }
}
