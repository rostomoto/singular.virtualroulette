﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Core.Base
{
    public interface IEntityBase
    {
        List<INotification> Events { get; }
    }
}
