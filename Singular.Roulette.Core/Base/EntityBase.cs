﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Core.Base
{
    public abstract class EntityBase : IEntityBase
    {
        public List<INotification> Events { get; } = new List<INotification>();
    }
}
