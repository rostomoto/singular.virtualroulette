﻿using MediatR;
using Singular.Roulette.Core.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Core.Bets.DomainEvents
{
    public class BetPlacedDomainEvent : INotification
    {
        public Bet Bet { get; }

        public BetPlacedDomainEvent(Bet bet)
        {
            this.Bet = bet;
        }
    }
}
