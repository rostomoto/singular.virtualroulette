﻿using MediatR;
using Singular.Roulette.Core.Base;
using Singular.Roulette.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Core.Bets
{
    public class Bet : EntityBase
    {
        public int Id { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }

        public string SpinId { get; set; }

        public string BetDetailsAsJSON { get; set; }

        public int BetAmountInCents { get; set; }

        public int EstimateWinInCents { get; set; }

        public string WinningNumber { get; set; }

        public DateTime BetCreateDate { get; set; }

        public DateTime? BetSettleDate { get; set; }
        public BetStatus Status { get; set; }
    }

    public enum BetStatus
    {
        Created = 1,
        SettledAndWon = 2,
        SettledAndLost = 3,

        Other = 99,
    }
}
