﻿using Microsoft.EntityFrameworkCore;
using Singular.Roulette.Core.Bets;
using Singular.Roulette.Core.Jackpots;
using Singular.Roulette.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Interfaces
{
    public interface IDatabaseService
    {
        public DbSet<Bet> Bets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Jackpot> Jackpots { get; set; }

        Task Save();
    }
}
