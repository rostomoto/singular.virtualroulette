﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Interfaces
{
    public interface IRandomGenerator
    {
        int Generate();
    }
}
