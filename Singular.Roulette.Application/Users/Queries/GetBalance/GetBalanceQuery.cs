﻿using Singular.Roulette.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Users.Queries.GetBalance
{
    public class GetBalanceQuery : IGetBalanceQuery
    {
        private readonly IDatabaseService _database;

        public GetBalanceQuery(IDatabaseService database)
        {
            _database = database;
        }

        public BalanceDetail Execute(int userId)
        {
            BalanceDetail result = new BalanceDetail();
            long? userBalance = _database.Users.First(x => x.Id == userId)?.BalanceInCents;
            result.Amount = userBalance;
            return result;
        }
    }
}
