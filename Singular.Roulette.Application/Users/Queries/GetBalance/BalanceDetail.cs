﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Users.Queries.GetBalance
{
    public class BalanceDetail
    {
        public long? Amount { get; set; }
    }
}
