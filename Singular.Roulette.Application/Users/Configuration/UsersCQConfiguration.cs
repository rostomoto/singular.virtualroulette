﻿using Microsoft.Extensions.DependencyInjection;
using Singular.Roulette.Application.Users.Queries.GetBalance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Users.Configuration
{
    public static class UsersCQConfiguration
    {
        public static IServiceCollection AddUsersCQDiConfiguration(this IServiceCollection services)
        {
            services.AddScoped<IGetBalanceQuery, GetBalanceQuery>();
            return services;
        }
    }
}
