﻿using Microsoft.Extensions.DependencyInjection;
using Singular.Roulette.Application.Bets.Commands.CreateBet;
using Singular.Roulette.Application.Bets.Commands.CreateBet.Factory;
using Singular.Roulette.Application.Bets.Queries.GetBetsForUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Configuration
{
    public static class BetsCQConfiguration
    {
        public static IServiceCollection AddBetsCQDiConfiguration(this IServiceCollection services)
        {
            services.AddScoped<ICreateBetCommand, CreateBetCommand>();
            services.AddScoped<IBetFactory, BetFactory>();
            services.AddScoped<IGetBetsForUserQuery, GetBetsForUserQuery>();
            return services;
        }
    }
}
