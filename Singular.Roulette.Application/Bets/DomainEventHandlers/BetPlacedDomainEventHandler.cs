﻿using MediatR;
using Singular.Roulette.Application.Interfaces;
using Singular.Roulette.Core.Bets.DomainEvents;
using Singular.Roulette.Core.Jackpots;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.DomainEventHandlers
{
    public class BetPlacedDomainEventHandler : INotificationHandler<BetPlacedDomainEvent>
    {
        private readonly IDatabaseService _database;
        private readonly double _tax = 0.01;
        public BetPlacedDomainEventHandler(IDatabaseService database)
        {
            _database = database;
        }

        public async Task Handle(BetPlacedDomainEvent notification, CancellationToken cancellationToken)
        {
            try
            {
                int betAmountInCents = notification.Bet.BetAmountInCents;
                Jackpot jackpot = _database.Jackpots.FirstOrDefault(x => x.Type == JackpotType.VirtualRoullete);
                if (jackpot != null)
                {
                    jackpot.TotalJackpotAmountInCentsAsString += (long)(_tax * betAmountInCents);
                    jackpot.LastModifiedDate = DateTime.UtcNow;
                    _database.Jackpots.Update(jackpot);
                    await _database.Save();
                }
            }
            catch(Exception ex)
            {
                //Log
            }
        }
    }
}
