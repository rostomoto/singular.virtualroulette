﻿using Singular.Roulette.Application.Interfaces;
using Singular.Roulette.Core.Bets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Queries.GetBetsForUser
{
    public class GetBetsForUserQuery : IGetBetsForUserQuery
    {
        private readonly IDatabaseService _database;

        public GetBetsForUserQuery(IDatabaseService database)
        {
            _database = database;
        }

        public List<BetDetail> Execute(int userId, int skip = 0, int take = 50)
        {
            List<BetDetail> result = new List<BetDetail>();

            List<Bet> bets = _database.Bets.Where(x => x.UserId == userId).OrderByDescending(x => x.BetCreateDate).Skip(skip).Take(take).ToList();

            bets.ForEach(x => result.Add(new BetDetail(x)));

            return result;
        }
    }
}
