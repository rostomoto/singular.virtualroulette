﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Queries.GetBetsForUser
{
    public interface IGetBetsForUserQuery
    {
        List<BetDetail> Execute(int userId, int skip = 0, int take = 50);
    }
}
