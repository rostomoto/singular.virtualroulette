﻿using Singular.Roulette.Core.Bets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Queries.GetBetsForUser
{
    public class BetDetail
    {

        public BetDetail()
        {

        }

        public BetDetail(Bet bet)
        {
            Id = bet.Id;
            UserId = bet.UserId;
            SpinId = bet.SpinId;
            BetAmountInCents = bet.BetAmountInCents;
            EstimateWinInCents = bet.EstimateWinInCents;
            BetCreateDate = bet.BetCreateDate;
        }

        public int Id { get; set; }

        public int UserId { get; set; }

        public string SpinId { get; set; }

        public int BetAmountInCents { get; set; }

        public int EstimateWinInCents { get; set; }

        public DateTime BetCreateDate { get; set; }
    }
}
