﻿using Singular.Roulette.Core.Bets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet.Factory
{
    public interface IBetFactory
    {
        Bet Create(int userId, string betDetailsAsJSON, int betAmountInCents);
    }
}
