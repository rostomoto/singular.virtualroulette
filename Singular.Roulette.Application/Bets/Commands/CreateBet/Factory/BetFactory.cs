﻿using MediatR;
using Singular.Roulette.Core.Bets;
using Singular.Roulette.Core.Bets.DomainEvents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet.Factory
{
    public class BetFactory : IBetFactory
    {
        public Bet Create(int userId, string betDetailsAsJSON, int betAmountInCents)
        {
            Bet result = new Bet()
            {
                BetAmountInCents = betAmountInCents,
                UserId = userId,
                BetDetailsAsJSON = betDetailsAsJSON,
                BetCreateDate = DateTime.UtcNow,
                SpinId = Guid.NewGuid().ToString(),
                Status = BetStatus.Created,
                BetSettleDate = null,
                EstimateWinInCents = 0,
                WinningNumber = null,
            };
            result.Events.Add(new BetPlacedDomainEvent(result));
            return result;
            
        }

    }
}
