﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet
{
    public interface ICreateBetCommand
    {
        Task<CreateBetResult> Execute(CreateBetModel model);
    }
}
