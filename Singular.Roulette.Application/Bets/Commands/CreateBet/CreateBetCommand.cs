﻿using ge.singular.roulette;
using Singular.Roulette.Application.Bets.Commands.CreateBet.Factory;
using Singular.Roulette.Application.Interfaces;
using Singular.Roulette.Core.Bets;
using Singular.Roulette.Core.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet
{
    public class CreateBetCommand : ICreateBetCommand
    {
        private readonly IDatabaseService _database;
        private readonly IBetFactory _factory;
        private readonly IRandomGenerator _randomGenerator;

        public CreateBetCommand(IDatabaseService database, IBetFactory factory, IRandomGenerator randomGenerator)
        {
            _database = database;
            _factory = factory;
            _randomGenerator = randomGenerator;
        }

        public async Task<CreateBetResult> Execute(CreateBetModel model)
        {
            CreateBetResult result = new CreateBetResult();

            //Validate User Balance
            User user = _database.Users.First(x => x.Id == model.UserId);
            if(user == null || user.BalanceInCents < model.BetAmountInCents)
            {
                result.Status = CreateBetStatus.NotEnoughBalance;
                return result;
            }

            //Bet validation and handling invalid bet format
            IsBetValidResponse isBetValidResponse = CheckBets.IsValid(model.BetDetailsAsJSON);
            if (!isBetValidResponse.getIsValid())
            {
                result.Status = CreateBetStatus.InvalidBet;
                return result;
            }

            //Create and save unsettled Bet
            Bet bet = _factory.Create(model.UserId, model.BetDetailsAsJSON, model.BetAmountInCents);
            _database.Bets.Add(bet);
            user.BalanceInCents -= model.BetAmountInCents;
            await _database.Save();

            //Simulate outcome of bet
            int winningNumber = _randomGenerator.Generate();
            int estimateWinInCents = CheckBets.EstimateWin(bet.BetDetailsAsJSON, winningNumber);

            //Update bet and result depending on outcome
            if(estimateWinInCents == 0)
            {
                bet.Status = BetStatus.SettledAndLost;
                result.Status = CreateBetStatus.Lost;
            }
            else
            {
                bet.Status = BetStatus.SettledAndWon;
                result.Status = CreateBetStatus.Won;
            }

            bet.EstimateWinInCents = estimateWinInCents;
            bet.BetSettleDate = DateTime.UtcNow;
            _database.Bets.Update(bet);
            await _database.Save();

            result.BetDetails = new CreateBetResultDetails(bet);
            return result;
        }
    }
}
