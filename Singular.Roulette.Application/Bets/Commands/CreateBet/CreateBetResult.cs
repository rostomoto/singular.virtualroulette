﻿using Singular.Roulette.Core.Bets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet
{
    public class CreateBetResult
    {
        public CreateBetStatus Status { get; set; }

        public CreateBetResultDetails BetDetails { get; set; }

    }

    public class CreateBetResultDetails
    {
        public CreateBetResultDetails()
        {

        }

        public CreateBetResultDetails(Bet bet)
        {
            Id = bet.Id;
            UserId = bet.UserId;
            SpinId = bet.SpinId;
            BetDetailsAsJSON = bet.BetDetailsAsJSON;
            BetAmountInCents = bet.BetAmountInCents;
            EstimateWinInCents = bet.EstimateWinInCents;
            WinningNumber = bet.WinningNumber;
            BetCreateDate = bet.BetCreateDate;
        }


        public int Id { get; set; }

        public int UserId { get; set; }

        public string SpinId { get; set; }

        public string BetDetailsAsJSON { get; set; }

        public int BetAmountInCents { get; set; }

        public int EstimateWinInCents { get; set; }

        public string WinningNumber { get; set; }

        public DateTime BetCreateDate { get; set; }
    }

    public enum CreateBetStatus
    {
        Accepted = 1,
        Won = 2,
        Lost = 3,
        NotEnoughBalance = 98,
        InvalidBet = 99,
    }

}
