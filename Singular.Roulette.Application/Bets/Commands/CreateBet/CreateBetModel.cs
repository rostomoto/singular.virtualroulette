﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Application.Bets.Commands.CreateBet
{
    public class CreateBetModel
    {

        public int UserId { get; set; }

        public string BetDetailsAsJSON { get; set; }

        public int BetAmountInCents { get; set; }



    }
}
