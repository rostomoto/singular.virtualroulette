﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Bets
{
    public class CreateBetRequest
    {

        public string BetDetailsAsJSON { get; set; }

        public int BetAmountInCents { get; set; }
    }
}
