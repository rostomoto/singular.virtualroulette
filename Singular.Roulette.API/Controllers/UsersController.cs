﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Singular.Roulette.Application.Users.Queries.GetBalance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IGetBalanceQuery _getBalanceQuery;

        public UsersController(IGetBalanceQuery getBalanceQuery)
        {
            _getBalanceQuery = getBalanceQuery;
        }



        // GET: api/<UsersController>
        [HttpGet("Balance")]
        [Authorize]
        public async Task<IActionResult> Get()
        {
            int userId = GetUserId();
            var result = _getBalanceQuery.Execute(userId);
            return Ok(result);
        }

        private int GetUserId()
        {
            int userId = int.Parse(User?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
            return userId;
        }
    }
}
