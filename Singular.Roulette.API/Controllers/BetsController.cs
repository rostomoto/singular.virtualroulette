﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Singular.Roulette.API.Bets;
using Singular.Roulette.Application.Bets.Commands.CreateBet;
using Singular.Roulette.Application.Bets.Queries.GetBetsForUser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BetsController : ControllerBase
    {
        private readonly IGetBetsForUserQuery _getBetsForUserQuery;
        private readonly ICreateBetCommand _createBetCommand;

        public BetsController(IGetBetsForUserQuery getBetsForUserQuery, ICreateBetCommand createBetCommand)
        {
            _getBetsForUserQuery = getBetsForUserQuery;
            _createBetCommand = createBetCommand;
        }



        // GET: api/<BetsController>
        [HttpGet]
        [Authorize]
        public async Task<IActionResult> Get(int page = 1, int perPage = 50)
        {
            int userId = GetUserId();
            var result = _getBetsForUserQuery.Execute(userId, perPage * (page - 1), perPage);
            return Ok(result);
        }


        // POST api/<BetsController>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] CreateBetRequest createBetModel)
        {
            await _createBetCommand.Execute(new CreateBetModel()
            {
                UserId = GetUserId(),
                BetAmountInCents = createBetModel.BetAmountInCents,
                BetDetailsAsJSON = createBetModel.BetDetailsAsJSON,

            });
            return Ok();
        }

        private int GetUserId()
        {
            int userId = int.Parse(User?.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
            return userId;
        }
    }
}
