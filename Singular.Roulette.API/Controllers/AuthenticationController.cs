﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Singular.Roulette.API.Authentication;
using Singular.Roulette.API.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IUsersActivityCache _usersActivityCache;
        public AuthenticationController(IAuthenticationService authenticationService, IUsersActivityCache usersActivityCache)
        {
            _authenticationService = authenticationService;
            _usersActivityCache = usersActivityCache;
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] LoginRequest loginRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            LoginResponse authResult = await _authenticationService.Login(loginRequest);
            if (authResult.IsSuccess)
            {
                return Ok(authResult);
            }

            return BadRequest("Invalid credentials");
        }

        [HttpPost]
        [Route("refresh-token")]
        public async Task<IActionResult> RefreshToken(TokenModel tokenModel)
        {
            if (tokenModel is null)
            {
                return BadRequest("Invalid client request");
            }

            string accessToken = tokenModel.AccessToken;
            string refreshToken = tokenModel.RefreshToken;

            try
            {
                var user = await _authenticationService.GetUserFromExpiredToken(accessToken);

                if (user == null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.UtcNow)
                {
                    return BadRequest("Invalid access token or refresh token");
                }

                if (!HasUserActivity(user.Id))
                {
                    return Unauthorized();
                }

                var newAccessToken = _authenticationService.CreateToken(user);
                var newRefreshToken = _authenticationService.GenerateRefreshToken();

                await _authenticationService.UpdateUsersRefreshToken(user.Id, newRefreshToken);

                return new ObjectResult(new
                {
                    accessToken = newAccessToken,
                    refreshToken = newRefreshToken
                });
            }
            catch(Exception ex)
            {
                return Unauthorized();
            }




            
        }

        private bool HasUserActivity(int userId)
        {
            bool result = _usersActivityCache.IsUserActive(userId.ToString());
            return result;
        }


        // GET: api/<AuthenticationController>
        [HttpGet]
        public async Task<IActionResult> GetVersion()
        {
            string result = "1.0.0";
            return Ok(result);
        }

    }
}
