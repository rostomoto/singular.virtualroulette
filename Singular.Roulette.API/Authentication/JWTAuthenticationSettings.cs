﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Authentication
{
    public class JWTAuthenticationSettings
    {
        public string Secret { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int ExpirationTimeInMinutes { get; set; }
        public int RefreshTokenValidityInDays { get; set; }
    }
}
