﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Singular.Roulette.Core.Users;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Authentication
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly JWTAuthenticationSettings _authenticationSettings;
        private readonly UserManager<User> _userManager;
        public AuthenticationService(UserManager<User> userManager, IOptions<JWTAuthenticationSettings> authenticationSettings)
        {
            _userManager = userManager;
            _authenticationSettings = authenticationSettings.Value;
        }

        public async Task<LoginResponse> Login(LoginRequest loginRequest)
        {
            User user = await _userManager.FindByNameAsync(loginRequest.Username);
            if(user == null)
            {
                return WrongCredentials();
            }
            bool isCorrectPassword = await _userManager.CheckPasswordAsync(user, loginRequest.Password);
            if(!isCorrectPassword)
            {
                return WrongCredentials();
            }

            //var claim = new[]
            //{
            //    new Claim(ClaimTypes.Name, user.UserName),
            //    //new Claim(ClaimTypes.Role, user.Role),
            //    new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
            //    //new Claim(ClaimTypes.Thumbprint, user.LoggedInToken ?? string.Empty),
            //};

            //var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.Secret));
            //var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //var jwtToken = new JwtSecurityToken(
            //    _authenticationSettings.Issuer,
            //    _authenticationSettings.Audience,
            //    claim,
            //    expires: DateTime.Now.AddMinutes(_authenticationSettings.ExpirationTimeInMinutes),
            //    signingCredentials: signingCredentials
            //);
            //string token = new JwtSecurityTokenHandler().WriteToken(jwtToken);

            string token = CreateToken(user);
            string refreshToken = GenerateRefreshToken();
            user.RefreshToken = refreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(_authenticationSettings.RefreshTokenValidityInDays);
            await _userManager.UpdateAsync(user);


            LoginResponse result = new LoginResponse()
            {
                IsSuccess = true,
                Token = token,
                RefreshToken = refreshToken
            };

            return result;
        }

        public string CreateToken(User user)
        {
            var claim = new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                //new Claim(ClaimTypes.Role, user.Role),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                //new Claim(ClaimTypes.Thumbprint, user.LoggedInToken ?? string.Empty),
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.Secret));
            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                _authenticationSettings.Issuer,
                _authenticationSettings.Audience,
                claim,
                expires: DateTime.Now.AddMinutes(_authenticationSettings.ExpirationTimeInMinutes),
                signingCredentials: signingCredentials
            );
            string token = new JwtSecurityTokenHandler().WriteToken(jwtToken);
            return token;
        }

        public string GenerateRefreshToken()
        {
            var randomNumber = new byte[64];
            using var rng = RandomNumberGenerator.Create();
            rng.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }

        public async Task<User> GetUserFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                //ValidateAudience = false,
                //ValidateIssuer = false,
                //ValidateIssuerSigningKey = true,
                //IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.Secret)),
                //ValidateLifetime = false
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_authenticationSettings.Secret)),
                ValidateIssuer = false,
                ValidateAudience = false,
                ValidIssuer = _authenticationSettings.Issuer,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
            if (securityToken is not JwtSecurityToken jwtSecurityToken || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token");

            string username = principal.Identity.Name;
            var user = await _userManager.FindByNameAsync(username);
            return user;
        }

        public async Task UpdateUsersRefreshToken(int userId, string newRefreshToken)
        {
            User user = _userManager.Users.FirstOrDefault(x => x.Id == userId);
            if(user == null)
            {
                return;
            }
            user.RefreshToken = newRefreshToken;
            user.RefreshTokenExpiryTime = DateTime.UtcNow.AddDays(_authenticationSettings.RefreshTokenValidityInDays);
            await _userManager.UpdateAsync(user);
        }

        private LoginResponse WrongCredentials()
        {
            LoginResponse response = new LoginResponse()
            {
                IsSuccess = false,
                Token = string.Empty,
            };
            return response;
        }

    }
}
