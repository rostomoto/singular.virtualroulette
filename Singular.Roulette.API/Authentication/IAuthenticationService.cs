﻿using Singular.Roulette.Core.Users;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Authentication
{
    public interface IAuthenticationService
    {
        Task<LoginResponse> Login(LoginRequest loginRequest);
        string CreateToken(User user);
        string GenerateRefreshToken();
        Task<User> GetUserFromExpiredToken(string token);
        Task UpdateUsersRefreshToken(int userId, string newRefreshToken);
    }
}
