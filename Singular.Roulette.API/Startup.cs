using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Singular.Roulette.API.Authentication;
using Singular.Roulette.API.Users;
using Singular.Roulette.Application.Bets.Configuration;
using Singular.Roulette.Application.Bets.DomainEventHandlers;
using Singular.Roulette.Application.Users.Configuration;
using Singular.Roulette.Core.Users;
using Singular.Roulette.Persistence.DataAccess;
using Singular.Roulette.Persistence.DataAccess.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMediatR(typeof(BetPlacedDomainEventHandler).Assembly);
            services.AddMemoryCache();
            services.AddScoped<IAuthenticationService, AuthenticationService>();
            services.AddScoped<IUsersActivityCache, UsersActivityCache>();
            services.AddBetsCQDiConfiguration();
            services.AddUsersCQDiConfiguration();
            services.AddDataAccessConfiguration(Configuration.GetConnectionString("default"));
            AddUserManager(services);
            AddJWTAuth(services, Configuration);
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Singular.Roulette.API", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Name = "Authorization",
                    Type = SecuritySchemeType.Http,
                    Scheme = "Bearer",
                    BearerFormat = "JWT",
                    In = ParameterLocation.Header,
                    Description = "Enter your valid token in the text input below.\r\n\r\nExample: \"Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\"",
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            }
                        },
                        new string[] {}
                    }
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Singular.Roulette.API v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            ConfigureAuth(app);

             

            app.Use(async (context, next) =>
            {
                // Do work that can write to the Response.
                await next.Invoke();
                if(context.User.Identity != null)
                {
                    string userId = context.User.Claims.ToList().FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
                    if(userId != null)
                    {
                        using (var scope = app.ApplicationServices.CreateScope())
                        {
                            IUsersActivityCache usersAcivityCacheService = scope.ServiceProvider.GetRequiredService<IUsersActivityCache>();
                            usersAcivityCacheService.MarkUserActivity(userId);
                        }
                    }
                }
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        protected virtual void ConfigureAuth(IApplicationBuilder app)
        {
            app.UseAuthentication();
            app.UseAuthorization();
        }

        public IServiceCollection AddJWTAuth(IServiceCollection services, IConfiguration configuration)
        {
            var key = Encoding.UTF8.GetBytes(configuration["AuthorizationConstants:JWT_SECRET_KEY"]);

            services.Configure<ValidateUser>(opt =>
            {
                opt.Validate = bool.Parse(configuration["ValidateUser:Validate"]);
            });

            services.AddAuthorization();

            services.AddAuthentication(config =>
            {
                config.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidIssuer = configuration["AuthorizationConstants:Issuer"],
                };
                config.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });
            services.Configure<JWTAuthenticationSettings>(options =>
            {
                options.Audience = configuration["AuthorizationConstants:Issuer"];
                options.Issuer = configuration["AuthorizationConstants:Issuer"];
                options.Secret = configuration["AuthorizationConstants:JWT_SECRET_KEY"];
                options.ExpirationTimeInMinutes = int.Parse(configuration["AuthorizationConstants:ExpirationTimeInMinutes"]);
                options.RefreshTokenValidityInDays = int.Parse(configuration["AuthorizationConstants:RefreshTokenValidityInDays"]);
            });
            services.AddScoped<IAuthenticationService, AuthenticationService>();

            return services;
        }

        public IServiceCollection AddUserManager(IServiceCollection services)
        {
            services.AddIdentityCore<User>().AddEntityFrameworkStores<VirtualRouletteDBContext>();
            services.Configure<IdentityOptions>(options =>
            {
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;

                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
                options.Password.RequiredLength = 3;
                options.Password.RequiredUniqueChars = 1;

                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(20);
                options.Lockout.MaxFailedAccessAttempts = 10;
                options.Lockout.AllowedForNewUsers = true;

            });
            return services;
        }

    }
}
