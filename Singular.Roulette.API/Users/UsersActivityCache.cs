﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Users
{
    public class UsersActivityCache : IUsersActivityCache
    {
        private readonly IMemoryCache _cache;
        private readonly int AcceptableDelayInMinutes = 5;
        public UsersActivityCache(IMemoryCache memoryCache)
        {
            _cache = memoryCache;
        }

        public bool IsUserActive(string userId)
        {
            bool result = false;
            var key = string.Concat(userId, "_user");
            var userActivityExists = _cache.TryGetValue(key, out DateTime lastAcitivityDate);
            if (userActivityExists && lastAcitivityDate >= DateTime.UtcNow.AddMinutes(-AcceptableDelayInMinutes))
            {
                result = true;
            }
            return result;
        }

        public void MarkUserActivity(string userId)
        {
            var key = string.Concat(userId, "_user");
            DateTime lastAcitivityDate = DateTime.UtcNow;
            _cache.Set(key, lastAcitivityDate);
        }
    }
}
