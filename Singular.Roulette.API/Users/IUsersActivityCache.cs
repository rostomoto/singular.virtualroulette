﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Singular.Roulette.API.Users
{
    public interface IUsersActivityCache
    {
        void MarkUserActivity(string userId);
        bool IsUserActive(string userId);
    }
}
