﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Singular.Roulette.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Singular.Roulette.Persistence.Services;

namespace Singular.Roulette.Persistence.DataAccess.Configuration
{
    public static class ConfigureDataAccess
    {
        public static IServiceCollection AddDataAccessConfiguration(this IServiceCollection services, string dbConnection)
        {
            services.AddDbContext<VirtualRouletteDBContext>(options => options.UseSqlServer(dbConnection));
            services.AddScoped<IDatabaseService, VirtualRouletteDBContext>();
            services.AddScoped<IRandomGenerator, RandomGenerator>();
            return services;
        }
    }
}
