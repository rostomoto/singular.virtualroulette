﻿using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Singular.Roulette.Application.Interfaces;
using Singular.Roulette.Core.Base;
using Singular.Roulette.Core.Bets;
using Singular.Roulette.Core.Jackpots;
using Singular.Roulette.Core.Users;
using Singular.Roulette.Persistence.Bets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Persistence.DataAccess
{

    public class VirtualRouletteDBContext : DbContext, IDatabaseService
    {
        public DbSet<Bet> Bets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Jackpot> Jackpots { get; set; }


        private readonly IMediator _mediator;

        public VirtualRouletteDBContext(DbContextOptions<VirtualRouletteDBContext> options, IMediator mediator) : base(options)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }

        public async Task Save()
        {
            this.SaveChanges();

            var domainEventEntities = ChangeTracker.Entries<IEntityBase>()
            .Select(x => x.Entity)
            .Where(x => x.Events != null && x.Events.Count > 0)
            .ToArray();
            foreach (var entity in domainEventEntities)
            {
                var events = entity.Events.ToArray();
                entity.Events.Clear();
                foreach (var domainEvent in events)
                {
                    await _mediator.Publish(domainEvent);
                }
            }


        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            var hasher = new PasswordHasher<IdentityUser>();
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = 1,
                    UserName = "myuser",
                    NormalizedUserName = "MYUSER",
                    Email = "myuser@gmail.com",
                    PasswordHash = hasher.HashPassword(null, "1234"),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    BalanceInCents = 999999999,
                }
            );

            modelBuilder.Entity<Jackpot>().HasData(
                new Jackpot
                {
                    Id = 1,
                    Type = JackpotType.VirtualRoullete,
                    LastModifiedDate = DateTime.UtcNow,
                }
            );

            modelBuilder.ApplyConfiguration(new BetConfiguration());

            modelBuilder.Entity<User>()
                .Property(p => p.Version)
                .IsRowVersion();

            modelBuilder.Entity<Jackpot>()
                .Property(p => p.Version)
                .IsRowVersion();

        }
    }
}
