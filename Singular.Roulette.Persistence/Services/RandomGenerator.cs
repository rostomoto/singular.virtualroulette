﻿using Singular.Roulette.Application.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singular.Roulette.Persistence.Services
{
    public class RandomGenerator : IRandomGenerator
    {
        public int Generate()
        {
            Random rnd = new Random();

            int result = rnd.Next(37);

            return result;
        }
    }
}
